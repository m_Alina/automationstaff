require 'rspec'
require 'capybara'
require 'capybara/rspec'
require 'site_prism'
require 'selenium-webdriver'

Capybara.app_host = 'http://demo.redmine.org'
require 'rspec'
require 'capybara'
require 'capybara/rspec'
require 'site_prism'
require 'selenium-webdriver'

Capybara.app_host = 'http://demo.redmine.org/'

RSpec.configure do |config|
  config.before :all do
    #setting Capybara driver
    Capybara.default_driver = :selenium
    Capybara.register_driver :selenium do |app|
      Capybara::Selenium::Driver.new(app, browser: :chrome)
    end
  end

  config.after :all do
    #setting Capybara driver to reset sessions after all tests are done
    Capybara.reset_sessions!
  end
end
