require_relative '../spec_helper.rb'

feature 'User visits loginpage', js: true do
  scenario 'Visitor successfully visits loginpage'do
    sleep 1
    visit '/login'
    sleep 1
    expect(page.current_url).to include 'http://demo.redmine.org/login'
    expect(page).to have_content 'Redmine demo'
    expect(page).to have_content 'Login'
    fill_in('Login', :with => 'alinamiroshnyk@gmail.com')
    fill_in('Password', :with => 'Cityfrog2016')
    click_button('Login')
    sleep 2
    expect(page).to have_content 'Sign out'
    expect(page).to have_content 'My account'


  end
end
