require_relative '../spec_helper.rb'

feature 'User visits homepage', js: true do
  scenario 'Visitor successfully visits homepage'do
    sleep 1
    visit '/'
    sleep 1
    expect(page.current_url).to include 'http://demo.redmine.org'
    expect(page).to have_content 'Redmine demo'

  end
end
